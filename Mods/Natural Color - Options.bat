@echo off
title %~n0
color f0

if exist "reshade-shaders\Shaders\TiltShift.fx" (
	goto menu
)
goto menu2

:menu
cls
echo What do you want to do?
echo.
echo [1] Install Natural Color Light Version (Color only, more performance).
echo [2] Change the key for the TiltShift effect.
echo [3] Uninstall Natural Color.
echo.
echo [4] Go to the Natural Color web page.
echo [5] Start Stardew Valley and exit.
echo [6] Exit
echo.
set /p choice=Type 1 to 6, then press Enter: 
if "%choice%"=="" goto menu
if "%choice%"=="1" goto light
if "%choice%"=="2" goto tiltshift
if "%choice%"=="3" goto uninstall
if "%choice%"=="4" start "" "https://www.nexusmods.com/stardewvalley/mods/1213"
if "%choice%"=="5" goto start
if "%choice%"=="6" exit
goto menu

:menu2
cls
echo What do you want to do?
echo.
echo [1] Install Natural Color Complete Version (Color, TiltShift, Chromatic Aberration).
echo [2] Uninstall Natural Color.
echo.
echo [3] Go to the Natural Color web page.
echo [4] Start Stardew Valley and exit.
echo [5] Exit
echo.
set /p choice=Type 1 to 5, then press Enter: 
if "%choice%"=="" goto menu2
if "%choice%"=="1" goto complete
if "%choice%"=="2" goto uninstall
if "%choice%"=="3" start "" "https://www.nexusmods.com/stardewvalley/mods/1213"
if "%choice%"=="4" goto start
if "%choice%"=="5" exit
goto menu2

:light
rd /q /s "reshade-shaders\Presets"
rd /q /s "reshade-shaders\Shaders"
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Light\Presets" "reshade-shaders\Presets"
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Light\Shaders" "reshade-shaders\Shaders"
goto menu2

:complete
rd /q /s "reshade-shaders\Presets"
rd /q /s "reshade-shaders\Shaders"
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color\Presets" "reshade-shaders\Presets"
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color\Shaders" "reshade-shaders\Shaders"
goto menu

:tiltshift
cls
echo What do you want to do?
echo.
echo [1]  Key F
echo [2]  Key F1
echo [3]  Key F2
echo [4]  Key F3
echo [5]  Key F4
echo [6]  Key F12
echo [7]  Key R
echo [8]  Key Space
echo [9]  Key T
echo [10] Key Tab
echo [11] Key V
echo [12] No key
echo.
echo [13] Back
echo [14] Exit
echo.
set /p choice=Type 1 to 14, then press Enter: 
if "%choice%"=="" goto tiltshift
if "%choice%"=="1" goto keyf
if "%choice%"=="2" goto keyf1
if "%choice%"=="3" goto keyf2
if "%choice%"=="4" goto keyf3
if "%choice%"=="5" goto keyf4
if "%choice%"=="6" goto keyf12
if "%choice%"=="7" goto keyr
if "%choice%"=="8" goto keyspace
if "%choice%"=="9" goto keyt
if "%choice%"=="10" goto keytab
if "%choice%"=="11" goto keyv
if "%choice%"=="12" goto nokey
if "%choice%"=="13" goto menu
if "%choice%"=="14" exit
goto tiltshift

:keyf
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyf1
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F1.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F1.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyf2
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F2.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F2.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyf3
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F3.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F3.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyf4
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F4.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F4.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyf12
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key F12.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key F12.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyr
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key R.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key R.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyspace
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key Space.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key Space.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyt
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key T.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key T.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keytab
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key Tab.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key Tab.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:keyv
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - Key V.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - Key V.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu
:nokey
xcopy /s /i /q /y "reshade-shaders\Options\Natural Color - Tilt-Shift Key\Natural Color - No Key.ini" "reshade-shaders\Presets"
move /y "reshade-shaders\Presets\Natural Color - No Key.ini" "reshade-shaders\Presets\Natural Color.ini"
goto menu

:uninstall
cls
rd /q /s "reshade-shaders"
del /f /q "ENBInjector.exe"
del /f /q "enbinjector.ini"
del /f /q "Natural_Color_Reshade.ini"
del /f /q "opengl32.dll"
del /f /q "opengl32.ini"
del /f /q "opengl32.log"
del /f /q "injector.exe"
del /f /q "ReShade32.dll"
del /f /q "ReShade32.ini"
del /f /q "ReShade32.log"
del /f /q "Injector.bat"
del /f /q "bcrypt.dll"
del /f /q "bcrypt.ini"
del /f /q "bcrypt-stardewmoddingapi.log"
del /f /q "bcrypt-stardew valley.log"
del /f /q "cryptsp.dll"
del /f /q "cryptsp.ini"
del /f /q "cryptsp-stardewmoddingapi.log"
del /f /q "cryptsp-stardew valley.log"
del /f /q "Uninstall Natural Color.bat"
del /f /q "Natural Color - Launcher.bat"
del /f /q "Natural Color - Options.bat"
exit

:start
if not exist "Stardew Valley.exe" (
	cls
	echo "Stardew Valley.exe" is missing, reinstall Natural Color in the root of Stardew Valley.
	pause > nul
	exit
)

if exist "StardewModdingAPI.exe" (
	start "" "StardewModdingAPI.exe"
	exit
)
start "" "Stardew Valley.exe"
exit
